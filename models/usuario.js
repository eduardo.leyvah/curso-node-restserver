const { Schema, model } = require('mongoose');

// Crear modelo de usuario
/*
    {
        nombre: 'Juan',
        correo: 'eduardo@gmail.com',
        password: '123456',
        img: 'usuario.jpg',
        role: 'ADMIN_ROLE'
        estado: false,
        google: true
    }
*/ 

const UsuarioSchema = new Schema({
    nombre: {
        type: String,
        required: [true, 'El nombre es necesario']
    },
    correo: {
        type: String,
        unique: true,
        required: [true, 'El correo es necesario']
    },
    password: {
        type: String,
        required: [true, 'La contraseña es obligatoria']
    },
    img: {
        type: String
    },
    role: {
        type: String,
        required: true,
        default: 'USER_ROLE',
        emun: ['ADMIN_ROLE', 'USER_ROLE']
    },
    estado: {
        type: Boolean,
        default: true
    },
    google: {  // Si el usuario se registra con google, no se le pide el password
        type: Boolean,
        default: false
    } 
});

UsuarioSchema.methods.toJSON = function() {
    const { __v, password, _id, ...usuario } = this.toObject();
    usuario.uid = _id;
    return usuario;
}


module.exports = model('Usuario', UsuarioSchema);
