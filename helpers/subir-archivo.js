const path = require('path');
const { v4: uuidv4 } = require('uuid');


const subirArchivo = ( files, extensionesValidas = ['png', 'jpg', 'gif', 'jpeg'], carpeta = '' ) => {

    return new Promise((resolve, reject) => {

        const { archivo } = files;
    
        const nombreCortado = archivo.name.split('.');
        const extensionArchivo = nombreCortado[nombreCortado.length - 1];
    
        
        // Validar la extension del archivo
        if ( !extensionesValidas.includes(extensionArchivo) ) {
            return reject(`La extensión ${extensionArchivo} no es válida, las extensiones válidas son ${extensionesValidas.join(', ')}`);
        }
    
        // Nombre de archivo personalizado
        const nombreTemp = `${ uuidv4() }.${ extensionArchivo }`;
    
        const uploadPath = path.join( __dirname,  '../uploads/', carpeta, nombreTemp );
    
        archivo.mv(uploadPath, (err) => {
            if (err) {
                reject(err);
            }
            
            resolve( nombreTemp );
        });
        
    });



}



module.exports = {
    subirArchivo
}