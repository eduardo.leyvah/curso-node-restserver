// const Role = require('../models/role');
// const Usuario = require('../models/usuario');
// const Categoria = require('../models/categoria');
const { Role,
        Usuario,
        Categoria,
        Producto } = require('../models');

const esRoleValido = async(role = '') => {
    const existeRol = await Role.findOne({ role });
    if (!existeRol) {
       throw new Error(`El rol ${role} no está registrado en la base de datos`);
    }
}

const emailExiste = async(correo = '') => {
    const existeCorreo = await Usuario.findOne({ correo });
    if (existeCorreo) {
         throw new Error(`El correo ${correo} ya está registrado en la base de datos`);
    }
}

const existeUsuarioById = async(id) => {
    const existeUsuario = await Usuario.findById(id);
    if (!existeUsuario) {
        throw new Error(`El usuario con el id ${id} no existe en la base de datos`);
    }
}

const existeCategoriaById = async(id) => {
    const existeCategoria = await Categoria.findById(id);
    if (!existeCategoria) {
        throw new Error(`La categoría con el id ${id} no existe en la base de datos`);
    }
}

const existeProductoById = async(id) => {
    const existeProducto = await Producto.findById(id);
    if (!existeProducto) {
        throw new Error(`El producto con el id ${id} no existe en la base de datos`);
    }
}

const coleccionesPermitidas = (coleccion = '', colecciones = []) => {

    const incluida = colecciones.includes(coleccion);

    if (!incluida) {
        throw new Error(`La colección ${coleccion} no está permitida, las permitidas son: ${colecciones.join(', ')}`);
    }

    return true;
 
}


module.exports = {
    esRoleValido,
    emailExiste,
    existeUsuarioById,
    existeCategoriaById,
    existeProductoById,
    coleccionesPermitidas
}