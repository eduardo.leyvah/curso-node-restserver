const { response } = require('express');
const bcryptjs = require('bcryptjs');

const Usuario = require('../models/usuario');

const { generarJWT } = require('../helpers/generarJWT');
const { googleVerify } = require('../helpers/google-verify');


const login = async (req, res) => {

    const { correo, password } = req.body; 

    try {

        // Verificar que el correo exista
        const usuarioDB = await Usuario.findOne({ correo });   
        
        if(!usuarioDB) {
            return res.status(400).json({
                msg: 'Usuario / Contraseña no son correctos - correo'
            });
        }
         
        // SI el estado del usuario está activo
        if( !usuarioDB.estado) {
            return res.status(400).json({
                msg: 'Usuario / Contraseña no son correctos - estado: false'
            });
        }

        // Verificar la contraseña
        const validPassword = bcryptjs.compareSync(password, usuarioDB.password);
        if( !validPassword ) {
            return res.status(400).json({
                msg: 'Usuario / Contraseña no son correctos - password'
            });
        }

        // Generar el JWT
        const token = await generarJWT( usuarioDB.id );


        res.json({
            usuarioDB,
            token
        });
        
    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg: 'Error inesperado'
        }); 
    }


}

const googleSignIn = async (req, res =  response) => {

    const { id_token } = req.body;

    
    try {
        
        const { nombre, correo, img } = await googleVerify( id_token );

        const usuarioDB = await Usuario.findOne({ correo });

        if( !usuarioDB ) {
            
            // Crear el nuevo usuario
            const data = {
                nombre,
                correo,
                password: ':)',
                img,
                google: true
            }

            const usuario = new Usuario(data);

            // Guardar el usuario
            await usuario.save();

        }

        // Si el usuario en DB
        if( !usuarioDB.estado ) {
            return res.status(401).json({
                msg: 'Hable con el administrador, el usuario está bloqueado'
            });
        }

        // Generar el JWT
        const token = await generarJWT( usuarioDB.id );

        res.json({
            usuarioDB,
            token
        });

    } catch (error) {

        res.status(400).json({
            msg: 'Token de Google no es válido'
        }); 

        
    }
    
    
    // const { googleId, nombre, correo, foto } = req.body;

    // try {

    //     // Verificar que el correo exista
    //     const usuarioDB = await Usuario.findOne({ correo });   
        
    //     if(usuarioDB) {
    //         return res.status(400).json({
    //             msg: 'El correo ya está registrado'
    //         });
    //     }

    //     // Crear el nuevo usuario
    //     const usuario = new Usuario({
    //         googleId,
    //         nombre,
    //         correo,
    //         foto,
    //         password: ':)'
    //     });

    //     // Guardar el usuario
    //     await usuario.save();

    //     // Generar el JWT
    //     const token = await generarJWT( usuario.id );

    //     res.json({
    //         usuario,
    //         token
    //     });

    // } catch (error) {
    //     console.log(error);
    //     res.status(500).json({
    //         msg: 'Error inesperado'
    //     }); 
    // }


}


module.exports = {
    login,
    googleSignIn
}