const { response } = require("express");

const { Producto } = require('../models');


// obtenerProductos - paginado - total - populado
const obtenerProductos = async(req, res = response) => {

    const { desde = 0, limite = 5 } = req.query;
    const query = { estado: true };

    const [total, productos] = await Promise.all([
        Producto.count(query),
        Producto.find(query)
            .skip(Number(desde))
            .limit(Number(limite))
            .populate('usuario', 'nombre correo role')
            .populate('categoria', 'nombre')
    ]);

    res.json({
        total,
        productos
    });


};

// obtenerProducto - por id - populado - estado = true
const obtenerProducto = async(req, res = response) => {

    const { id } = req.params;

    const producto = await Producto.findById(id)
                                   .populate('usuario', 'nombre')
                                   .populate('categoria', 'nombre');
        
    if (!producto) {
        return res.status(400).json({
            msg: 'El producto no existe'
        });
    }

    res.json(producto);

}


// Crear un producto
const crearProducto = async(req, res = response) => {
    
    const { estado, usuario, ...body } = req.body;

    const productoDB = await Producto.findOne({nombre: body.nombre});

    if ( productoDB ) {
        return res.status(400).json({
            msg: `El producto ${productoDB.nombre}, ya existe`
        });
    }

    // Generar la data a guardar
    const data = {
        ...body,
        nombre: body.nombre.toUpperCase(),
        usuario: req.usuario._id
    };

    // Crear el producto
    const producto = new Producto(data);

    // Guardar el producto
    await producto.save();

    res.status(201).json(producto);

}

// actaulizarProducto - por id
const actualizarProducto = async(req, res = response) => {

    const { id } = req.params;
    const { estado, usuario, ...data } = req.body;

    data.nombre = data.nombre.toUpperCase();
    data.usuario = req.usuario._id;

    const productoDB = await Producto.findByIdAndUpdate(id, data, { new: true }).populate('usuario', 'nombre correo role').populate('categoria', 'nombre');

    res.json(productoDB);

}

// Eliminar un producto - estado = false
const eliminarProducto = async(req, res = response) => {

    const { id } = req.params;

    const productoDB = await Producto.findByIdAndUpdate(id, { estado: false }, { new: true }).populate('usuario', 'nombre correo role').populate('categoria', 'nombre');

    res.json(productoDB);

}


module.exports = {
    crearProducto,
    obtenerProductos,
    obtenerProducto,
    actualizarProducto,
    eliminarProducto
}
