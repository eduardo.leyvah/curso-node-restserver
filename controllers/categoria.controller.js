const { response } = require("express");

const { Categoria } = require("../models");

// obtenerCatgorias - paginado - total - populado
const obtenerCategorias = async(req, res = response) => {
    
    const { desde = 0, limite = 5 } = req.query;
        const query = { estado: true };

        const [total, categorias] = await Promise.all([
            Categoria.count(query),
            Categoria.find(query)
                .skip(Number(desde))
                .limit(Number(limite))
                .populate('usuario', 'nombre correo role')
        ]);

        res.json({
            total,
            categorias
        });

};

// obtenerCategoria - por id - populado - estado = true
const obtenerCategoriaByID = async(req, res = response) => {
    
    const categoria = await Categoria.findById(req.params.id)
        .populate('usuario', 'nombre correo role');

    if (!categoria) {
        return res.status(400).json({
            msg: 'La categoría no existe'
        });
    }

    res.json(categoria);
}

// Crear una categoría
const crearCategoria = async(req, res = response) => {
    
    const nombre = req.body.nombre.toUpperCase();

    const categoriaDB = await Categoria.findOne({ nombre });

    if ( categoriaDB ) {
        return res.status(400).json({
            msg: `La categoría ${categoriaDB.nombre}, ya existe`
        });
    }

    // Generar la data a guardar
    const data = {
        nombre,
        usuario: req.usuario._id
    };

    // Crear la categoría
    const categoria = new Categoria(data);

    // Guardar la categoría
    await categoria.save();

    res.status(201).json(categoria);

}

// actaulizarCategoria - por id
const actualizarCategoria = async(req, res = response) => {

    const { id } = req.params;
    const { estado, usuario, ...data } = req.body;
    
    data.nombre = data.nombre.toUpperCase();
    data.usuario = req.usuario._id;

    const categoriaDB = await Categoria.findByIdAndUpdate(id, data, {new: true}).populate('usuario', 'nombre correo role');

    res.json(categoriaDB);

}

// Eliminar una categoría - estado = false
const eliminarCategoria = async(req, res = response) => {
    
    const { id } = req.params;

    const categoriaDB = await Categoria.findByIdAndUpdate(id, { estado: false }, {new: true}).populate('usuario', 'nombre correo role');

    res.json(categoriaDB);

}

module.exports = {
    obtenerCategorias,
    obtenerCategoriaByID,
    crearCategoria,
    actualizarCategoria,
    eliminarCategoria 
}