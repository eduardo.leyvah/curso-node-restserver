
const { Router } = require('express');
const { check } = require('express-validator');

// const { validarCampos } = require('../middlewares/validar-campos');
// const { validarJWT } = require('../middlewares/validar-jwt');
// const { esAdminRole, tieneRole } = require('../middlewares/validar-roles');
const {validarCampos, validarJWT, esAdminRole, tieneRole} = require('../middlewares');

const { esRoleValido, emailExiste, existeUsuarioById } = require('../helpers/db-validators');

const { getUsuarios, 
        putUsuarios, 
        postUsuarios, 
        deleteUsuarios, 
        patchUsuarios } = require('../controllers/users.controller');

const router = Router();


router.get('/', getUsuarios);

router.put('/:id',[
        check('id','No es un ID válido').isMongoId(),
        check('id').custom(existeUsuarioById),
        check('role').custom( esRoleValido ), 
        validarCampos
],putUsuarios);

router.post('/',[
        check('nombre', 'El nombre es obligatorio').not().isEmpty(),
        check('password', 'La contraseña es obligatoria y debe tener mínimo 6 caracteres').isLength({ min: 6 }),
        check('correo', 'El correo no es válido').isEmail(),
        check('correo').custom(emailExiste),
        //check('role', 'No es un rol válido').isIn(['ADMIN_ROLE', 'USER_ROLE']),
        check('role').custom( esRoleValido ),     
        validarCampos
], postUsuarios);

router.delete('/:id',[
        validarJWT,
        // esAdminRole,
        tieneRole('ADMIN_ROLE', 'VENTAS_ROLE'),
        check('id','No es un ID válido').isMongoId(),
        check('id').custom(existeUsuarioById),
        validarCampos
],deleteUsuarios);

router.patch('/', patchUsuarios);


module.exports = router;
