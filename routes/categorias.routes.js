const { Router } = require('express');
const { check } = require('express-validator');

const { validarJWT, validarCampos, esAdminRole } = require('../middlewares');

const { crearCategoria, 
        obtenerCategorias, 
        obtenerCategoriaByID, 
        actualizarCategoria, 
        eliminarCategoria} = require('../controllers/categoria.controller');

const { existeCategoriaById } = require('../helpers/db-validators');

const router = Router();

// Obtener todas las categorias - publico
router.get('/', obtenerCategorias);

// Obtener una categoria por id - publico
router.get('/:id', [
    check('id','No es un ID válido').isMongoId(),
    check('id').custom(existeCategoriaById),
    validarCampos
] ,obtenerCategoriaByID);

// Crear una categoria - privado - cualquier persona con un token valido
router.post('/',[ 
    validarJWT,
    check('nombre', 'El nombre es obligatorio').not().isEmpty(),
    validarCampos
], crearCategoria);

// Actualizar una categoria - privado - cualquier persona con un token valido
router.put('/:id', [
    validarJWT,
    check('id','No es un ID válido').isMongoId(),
    check('id').custom(existeCategoriaById),
    check('nombre', 'El nombre es obligatorio').not().isEmpty(),
    validarCampos
], actualizarCategoria);

// Eliminar una categoria - privado - Admin
router.delete('/:id',[
    validarJWT,
    esAdminRole,
    check('id','No es un ID válido').isMongoId(),
    check('id').custom(existeCategoriaById),
    validarCampos
], eliminarCategoria);




module.exports = router;